import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';

import Routes from './src/Routes';

class App extends Component {

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#1565C0"
                    barStyle="light-content"
                />
                <Routes/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
    }
});

export default App

/* End of file App.js */
/* Location: ./App.js */