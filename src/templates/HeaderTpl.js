import React, { Component } from 'react';
import { Header, Body, Title } from 'native-base';


export default class HeaderTpl extends Component {
    render() {
        return (
            <Header>
                <Body style={{alignItems:'center'}}>
                    <Title>MyApps</Title>
                </Body>
            </Header>
        );
    }
}


/* End of file HeaderTpl.js */
/* Location: ./src/templates/HeaderTpl.js */