import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, RefreshControl } from 'react-native';
import { Container, Content } from 'native-base';

import HeaderTpl from '../templates/HeaderTpl';
import FooterTpl from '../templates/FooterTpl';

import {getTest} from '../networking/Server';

class FlatListItem extends Component {
    
    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection:'column',
                paddingTop: 13,
                paddingRight: 13,
                paddingLeft: 13,
                paddingBottom: 5
                }}>
                <View style={{
                    flex: 1,
                    flexDirection:'row',
                    backgroundColor: 'white'
                    }}>
                    <Image source={{uri: this.props.item}}
                        style={{width: 130, height: 180,  borderWidth: 1, borderColor: '#d6d7da'}}
                        borderRadius={5} />
                    <View style={{
                        flex:1,
                        flexDirection:'column',
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingBottom: 5,
                        }}>

                        <Text style={styles.titleColumnPage}>{JSON.stringify(this.props.item)}</Text>
                    </View>
                </View>
                <View style={styles.borderView}></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    titleColumnPage: {
        color: '#212121',
        fontSize:18,
        fontWeight: 'bold',
    },  
    flatListItem: {
        color: '#212121',
        fontSize: 12
    },
    borderView: {
        height:1, 
        backgroundColor:'#d6d7da',
        marginTop: 20
    },
    'borderView:last-child': {
        height: '',
        backgroundColor: 'white'
    }
});

export default class Task3 extends Component {
    constructor(props){
        super(props);
        this.state = ({
            refreshing: false,
            listData : []
        });
    }

    componentDidMount(){
        this.refreshDataFromServer();
    }

    refreshDataFromServer = () => {
        this.setState({ refreshing: true });
        getTest().then((data) => {
            this.setState({ listData : data });
            this.setState({ refreshing: false });
        }).catch((error) => {
            this.setState({ listData: [] });
            this.setState({ refreshing: false });
        });
    }

    onRefresh = () => {
        this.refreshDataFromServer();
    }

    render() {
        return (
            <Container>
                <HeaderTpl/>
                    <Content>
                        <View style={{flex:1}}>
                            <FlatList
                                ref={"flatList"}
                                data={this.state.listData}
                                renderItem={({item, index}) => {
                                    //console.log(`Item = ${JSON.stringify(item)}, index = ${index}`);
                                    return (
                                        <FlatListItem 
                                            item={item} 
                                            index={index}>
                                        </FlatListItem>
                                    );
                                }}
                                keyExtractor={(item, index) => item}
                                refreshControl={
                                    <RefreshControl 
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh} 
                                    />
                                }>
                            </FlatList>
                        </View>
                    </Content>
                <FooterTpl/>
            </Container>
        );
    }
}

/* End of file Task3.js */
/* Location: ./src/pages/Task3.js */