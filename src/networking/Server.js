import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';


const urlApps = 'https://dog.ceo/api/breed/boxer/images';

async function getTest(){
    try{
        let response = await fetch(urlApps);
        let responseJson = await response.json();
        return responseJson.message;
    }
    catch(error){
        console.log(`Error is : ${error}`);
    }
}

export {getTest};

/* End of file Server.js */
/* Location: ./src/networking/Server.js */