import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';

import Task1 from './pages/Task1';
import Task2 from './pages/Task2';
import Task3 from './pages/Task3';

class Routes extends Component {

    constructor() {
        super();
    }

    render(){
        return(
            <Router>
                <Stack key="root" hideNavBar={true}>
                    <Scene key="task1" component={Task1} title="Task 1" initial={true} />
                    <Scene key="task2" component={Task2} title="Task 2" />
                    <Scene key="task3" component={Task3} title="Task 3" />
                </Stack>
            </Router>            
        );
    }
}

export default Routes

/* End of file Routes.js */
/* Location: ./src/Routes.js */