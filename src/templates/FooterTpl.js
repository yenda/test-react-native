import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Footer, FooterTab, Button } from 'native-base';
import {Actions} from 'react-native-router-flux';

export default class FooterTpl extends Component {
    render() {
        return (
            <Footer>
                <FooterTab>
                    <Button onPress={Actions.task1}>
                        <Text style={styles.title}>TASK SATU</Text>
                    </Button>
                    <Button onPress={Actions.task2}>
                        <Text style={styles.title}>TASK DUA</Text>
                    </Button>
                    <Button onPress={Actions.task3}>
                        <Text style={styles.title}>TASK TIGA</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}

const styles = StyleSheet.create({
    title : {
        color: '#ffffff',
    }
});

/* End of file FooterTpl.js */
/* Location: ./src/templates/FooterTpl.js */