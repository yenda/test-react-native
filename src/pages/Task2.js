import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image } from 'react-native';
import { Container, Content } from 'native-base';

import HeaderTpl from '../templates/HeaderTpl';
import FooterTpl from '../templates/FooterTpl';

import flatListData from '../data/flatListData';

class FlatListItem extends Component {
    
    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection:'column',
                paddingTop: 13,
                paddingRight: 13,
                paddingLeft: 13,
                paddingBottom: 5
                }}>
                <View style={{
                    flex: 1,
                    flexDirection:'row',
                    backgroundColor: 'white'
                    }}>
                    <Image source={{uri: this.props.item.url}}
                        style={{width: 130, height: 180,  borderWidth: 1, borderColor: '#d6d7da'}}
                        borderRadius={5} />
                    <View style={{
                        flex:1,
                        flexDirection:'column',
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingBottom: 5,
                        }}>

                        <Text style={styles.title}>{this.props.item.title}</Text>
                    </View>
                </View>
                <View style={styles.borderView}></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        color: '#212121',
        fontSize:18,
        fontWeight: 'bold',
    },  
    flatListItem: {
        color: '#212121',
        fontSize: 12
    },
    borderView: {
        height:1, 
        backgroundColor:'#d6d7da',
        marginTop: 20
    },
    'borderView:last-child': {
        height: '',
        backgroundColor: 'white'
    }
});

export default class Task2 extends Component {
    render() {
        return (
            <Container>
                <HeaderTpl/>
                    <Content>
                        <View style={{flex:1}}>
                            <FlatList
                                ref={"flatList"}
                                data={flatListData}
                                renderItem={({item, index}) => {
                                    return (
                                        <FlatListItem 
                                            item={item} 
                                            index={index}>
                                        </FlatListItem>
                                    );
                                }}
                                keyExtractor={(item, index) => item.title}
                            >
                            </FlatList>
                        </View>
                    </Content>
                <FooterTpl/>
            </Container>
        );
    }
}

/* End of file Task2.js */
/* Location: ./src/pages/Task2.js */